App
	- User

LoginForm
	- < onConnect

Site
	- Page
    - Services
    - fatures
    - currentFacture

Factures
	- > Factures

FactureDetail
	- > Id
	- > Facture

FactureEditForm
	- > Facture
    - > services
    - < onSubmit(facture, newFacture)

Services
    - > services
    - < onUpdate(service, NewService)
    - < onDelete(service)
    - < onCreate(service)

FactureCreateForm
    - > services
    - < onSubmit(newFacture)