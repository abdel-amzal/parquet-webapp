/**
 * Représente une erreur renvoyée par l'API
 */
export class ApiErrors {
    constructor(errors) {
        this.errors = errors
    }
}


/**
 * @param {string} endpoint 
 * @param {object} options 
 */
export async function apiFetch (endpoint, options = {}) {
    const response = await fetch('http://localhost:8080' + endpoint, {
        headers: {
            Accept: 'application/json'
        },
        ...options
    })
    if (response.status === 204) {
        return null;
    }
    const responseData = await response
    if (response.ok) {
        return responseData;
    } else {
        if (responseData.errors) {
            throw new ApiErrors(responseData.errors)
        }
    }
}

