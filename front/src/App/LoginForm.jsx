import React, { useState } from 'react'
import PropTypes from 'prop-types';
import { ApiErrors, apiFetch } from '../utils/api';

export function LoginForm({ onConnect }) {
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    const getUserFromToken = token => {
        if (token) {
            try {
                return JSON.parse(atob(token.split('.')[1]));
            } catch (error) {
                // ignore
            }
        }
        return null;
    };

    const handleSubmit = async function (e) {
        setError(null)
        setLoading(true)
        e.preventDefault()
        const data = new FormData(e.target)
        const dataUser = {
            username : data.get("username"),
            password : data.get("password")
        }
        try {
            const userToken = await apiFetch('/login', {
                method: 'POST',
                body: JSON.stringify(dataUser),
            })
            onConnect(userToken.headers.get('authorization'))
            const userFromToken = getUserFromToken(userToken.headers.get('authorization'))
            console.log("dcode :",userFromToken)
        } catch (e) {
            if (e instanceof ApiErrors) {
                setError(e.errors[0].message)
            } else {
                console.error(e)
            }
            setLoading(false)
        }
    }

    return <form className="container mt-4" onSubmit={handleSubmit}>
        <h2>Se connecter</h2>
        {error && <Alert>{error}</Alert>}
        <div className="form-group">
            <label htmlFor="username">Nom d'utilisateur</label>
            <input type="text" name="username" id="username" className="form-control" required />
        </div>
        <div className="form-group">
            <label htmlFor="password">Mot de passe</label>
            <input type="password" name="password" id="password" className="form-control" required />
        </div>
        <button disabled={loading} type="submit" className="btn btn-primary">Se connecter</button>
    </form>
}

LoginForm.propTypes = {
    onConnect: PropTypes.func.isRequired
}


function Alert({ children }) {
    return <div className="alert alert-danger">
        {children}
    </div>
}